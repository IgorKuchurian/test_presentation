# Test 

An application architecture for something

---

## Index

- Embedded frameworks as a way to structure code
- Work with custom input form
- Protocol oriented programming in practice
- JSON parsing implementation
- UI vertical carousel implementation

---

## Embedded frameworks

![Project Structure](assets/EmbeddedFrameworkPJ.png)

---

### Podfile:

```swift
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '9.0'
use_frameworks!

abstract_target 'Common' do

    target 'project' do
        pod 'Alamofire', '~> 4.3'
        pod 'AlamofireImage', '~> 3.1'
        pod 'FacebookCore', '~> 0.2.0'
        pod 'FacebookLogin', '~> 0.2.0'
        pod 'FacebookShare', '~> 0.2.0'
        pod 'KDCircularProgress', '~> 1.5.2'
        pod 'SwiftGifOrigin', '~> 1.6.1'
    end

    target 'Tools' do
        pod 'Log'
    end

end
```
---

### Pros

- Easy access for common code for different apps/targets (e.g.: Today extension, watchOS companion app, macOS app)
- Code encapsulation
- Quicker rebuild times for swift (if changes were made only inside framework)

+++

### Cons

- Xcode has hard time handling framework content
- Confuses namespaces
- Could be detrimental to performance

---

## Protocol oriented feature usage

```swift
public protocol ProfileCellRepresentable {
    var name: String { get }
    var userName: String { get }
    var id: String { get }
}

public extension ProfileCellRepresentable {
    func isSame(as anotherProfileRepresentable: ProfileCellRepresentable) ->  Bool {
        return id == anotherProfileRepresentable.id && type(of: self) == type(of: anotherProfileRepresentable)
    }
    
    var combinedName {
    return "\(name) \n (\(username))"
    }
    
}
```
---

## JSON parsing implementation 

```swift
public final class Team: AnyEntity {
    public var id: String
    public var name: String
    public var logo: Image
    public var shortName: String
    public var sportId: String

    public required init(_ raw: Any?) {
        let json: JSON = parse(raw)
        id = parse(json[.id])
        name = parse(json[.team])
        logo = Image(json[.logo])
        shortName = parse(json[.short_name])
        sportId = parse(json[.sport_id])
    }

    public var asJSON: JSON {
        var json = JSON()
        json[.id] = id
        json[.team] = name
        json[.logo] = logo.asJSON
        json[.short_name] = shortName
        json[.sport_id] = sportId
        return json
    }

}

```
+++

### Parse

```swift
public func parse<In, Out: Convertible>(_ input: In?) -> Out {
    return Out.cast(input)
}

public func parse<Out>(_ input: Any?, _ map: (Any) -> (Out)) -> [Out] {
    let array: [Any] = parse(input)
    let result = array.map(map)
    return result
}
```

+++

```swift

public protocol Convertible {
    static func cast<T>(_ input: T) -> Self
}

extension String: Convertible {

    public static func cast<T>(_ input: T) -> String {
        switch input {
        case let string as String:
            return string
        case let int as Int64:
            return int.description
        case let bool as Bool:
            return bool.description
        case let double as Double:
            return double.description
        default:
            return ""
        }
    }
}

```

---

## UI Vertical carousel




---

### Challenges

- Custom transition animation
- Dynamic content (timers)
- Different types of cells, 'active' cell has different size and content

---

```swift

public final class CarouselView: UIView {
    
    fileprivate let tableView: UITableView = {
        let table = UITableView()
        table.decelerationRate = UIScrollViewDecelerationRateNormal
        table.separatorStyle = .none
        table.allowsSelection = false
        return table
    }()
    
    public weak var delegate: CarouselViewDelegate?
    public var scrollRecessionRate: CGFloat = 0.17
    public var decelerationRate: CGFloat {
        get {
            return tableView.decelerationRate
        }
        set {
            tableView.decelerationRate = newValue
        }
    }
    }
```